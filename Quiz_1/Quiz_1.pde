void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, -(height/ 2.0 )/tan(PI * 30 / 180), 0, 0 ,0, 0, -1, 0);
 background(0);
}

int round=0;
void draw()
{
  float gauss=randomGaussian();
  float standardDeviation=floor(random(-350, 350));
  float mean=0;
 
  
  float xPos=(standardDeviation*gauss)+mean;
  float yPos=floor((random(-350,350)));
  float size=(random(10,100));

  
  noStroke();
  fill( random(0,255), random(0,255), random(0,255), random(0,255));
  circle(xPos,yPos,size);
  round++;
  
  if(round==1000)
  {
    setup();
    round=0;
  }
  
  
  
}
