class Walker
{
  float xPosition=Window.left/Window.right;
  float yPosition=Window.top/Window.bottom;
  
  float dt = 0;
  void render()
{
 
  noStroke();
  fill( random(255), random(255), random(255), random(255));
  circle(xPosition, yPosition, random(20,50));
}

void randomWalk()
{
  float n =noise(dt);
  float x = map(n, 0, Window.top, 0, Window.bottom);
  float y = map(n, 0, Window.left, 0, Window.right);
  int decision=floor(random(8));
   if (decision==0){
     yPosition+=x;
   }
   if (decision==1){
      yPosition-=y+(dt+10);
   }
   if (decision==2){
     xPosition+=x+(dt+10);
   }
   if (decision==3){
     xPosition-=x+(dt+10);
   }
   if (decision==4){
      yPosition+=y+(dt+10);
      xPosition+=x+(dt+10);
   }
   if (decision==5){
     yPosition+=y+(dt+10);
     xPosition-=x+(dt+10);
   }
   if (decision==6){
     yPosition-=y+(dt+10);
     xPosition-=x+(dt+10);
   }
   if (decision==7){
     yPosition-=y+(dt+10);
     xPosition+=x+(dt+10);
   }
   
 dt+=0.01f;
}
}
